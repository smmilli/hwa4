import java.util.*;

/** This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main(String[] param) {
      Lfraction f1 = new Lfraction (2L, 5L);

      System.out.println(f1.toString());

      System.out.println(valueOf(f1.toString()));

   }

   // TODO!!! instance variables here
   private long a;
   private long b;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      if (b < 1 ) throw new RuntimeException("Denominator must always be positive");
//      b = Math.abs(b);
      if (a == 0) b = 1;
//      int commonDivisor = greatestCommonDivisor(a,b);
      int commonDivisor2 = divisor(Math.abs(a),b);
      this.a = a/commonDivisor2;
      this.b = b/commonDivisor2;
   }

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
//      return 0L; // TODO!!!
      return a;
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() { 
      return b;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return Long.toString(a) + " / " + Long.toString(b);
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {

      if (m == null) return false;
      if (this.getClass() != m.getClass()) return false;
      return compareTo((Lfraction)m) == 0;


//      long a2 = ((Lfraction) m).getNumerator();
//      long b2 = ((Lfraction) m).getDenominator();
//      Lfraction compareto = new Lfraction(a2, b2);

//      return (this.a == compareto.a && this.b == compareto.b);

//      long a2 = ((Lfraction) m).getNumerator();
//      long b2 = ((Lfraction) m).getDenominator();
//      int commondi1 = greatestCommonDivisor(a2, b2);
//      int commondi0 = greatestCommonDivisor(a, b);
//      Lfraction comparable = new Lfraction(this.a/commondi0, this.b/commondi0);
//      Lfraction compareto = new Lfraction(a2/commondi1, b2/commondi1);
//
//      return (comparable.a == compareto.a && comparable.b == compareto.b);

//      return comparable.hashCode() == compareto.hashCode();
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Objects.hash(a,b);
//      return Long.toString(a).hashCode() * Long.toString(b).hashCode() * (a < 0 ? -1: 1);

   }

   public static int greatestCommonDivisor(long a1, long a2){
      if (a1 == a2) return (int) a1;
      if (a2 == 1) return 1;
      int common = 1;
      for (int i = 1; i <= a1 || i <= a2; i++) {
         if (Math.abs(a1) % i == 0 && Math.abs(a2) % i == 0){
            common = i;
         }
      }
      return common;
   }
   public static int divisor(double numerator, double denominator) {
      if (numerator == 0) {
         return 1;
      }
      //If next reminder is 0 return it.
      if (denominator % numerator == 0) {
         return (int) numerator;
      }
      //Recursive give back the reminder and number where it will be divided. If its reversed 4 % 7 return 4
      return divisor(denominator % numerator, numerator);
   }



   public long commonDenomitor(long b1, long b2){
      boolean whichOne = b1 >= b2;

      long common = whichOne ? b1: b2;

      if (whichOne){
         while(common % b2 != 0){
            common += b1;
         }
      } else {
         while (common % b1 != 0) {
            common += b2;
         }
      }
      return common;
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */

   public Lfraction plus (Lfraction m) {

      long common = commonDenomitor(b, m.getDenominator());
//      long common = b * m.b;
      long a1 = common/b * a + common/m.getDenominator() * m.getNumerator();
//      long a2 = common;
//      int commonDi = greatestCommonDivisor(a1, a2);
      return new Lfraction(a1, common);
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      long a1 = this.a *  m.a;
      long a2 = this.b * m.b;
//      int commonDi = greatestCommonDivisor(a1, a2);
//      return new Lfraction(a1/commonDi, a2/commonDi);
      return new Lfraction(a1, a2);

   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      if (a == 0){
         throw new RuntimeException("Cant inverse value 0");
      }

      if (a > 0) return new Lfraction(b,a);

     return new Lfraction(-b,Math.abs(a));
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(-a, b);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
//         long b2 = m.getDenominator();
//         long a2 = m.getNumerator();
//         long denom = commonDenomitor(b, b2);
//         long numer = denom/b*a - denom/b2*a2;
//         int commonDi = greatestCommonDivisor(numer, denom);
//         return new Lfraction(numer/commonDi, denom/commonDi);

         m = m.opposite();
         return this.plus(m);



   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      if (m.a == 0){
         throw new RuntimeException("Can't divide with 0");
      }
      m = m.inverse();
      return this.times(m);
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */


   @Override
   public int compareTo (Lfraction m) {
//      int commondi1 = greatestCommonDivisor(m.getNumerator(), m.getDenominator());
//      int commondi0 = greatestCommonDivisor(a, b);
//      Lfraction comparable = new Lfraction(this.a/commondi0, this.b/commondi0);
//      Lfraction compareto = new Lfraction(m.getNumerator()/commondi1, m.getDenominator()/commondi1);

      long a1 = this.a;
      long b1 = this.b;
      long a2 = m.a;
      long b2 = m.b;


      long eq = a1*b2 - a2*b1;
      if (eq == 0) return 0;
      return (eq > 0) ? 1: -1;


   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
     return new Lfraction(a,b);
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
      return a/b;
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
     return new Lfraction(a%b, b);
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return a * 1.0 / b;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      long result = Math.round(d * f);
      return new Lfraction(result, d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      Lfraction fra;
      String [] elements = s.trim().split("/");
      try {
         long a1 = Long.parseLong(elements[0].trim());
         long a2 = Long.parseLong(elements[1].trim());
         fra = new Lfraction(a1, a2);
      } catch (RuntimeException e) {
         throw new RuntimeException("Ivalid input: " + s);
      }

      return fra;
   }
}

